from typing import Deque, Dict
import discord
from discord.ext import commands
import collections
import re
import config

bot_token = config.BOT_TOKEN
buffer_length = config.BUFFER_LENGTH
max_tenor_count = config.MAX_TENOR_COUNT
post_delete_msg = config.POST_DELETE_MSG

intents = discord.Intents.default()
intents.messages = True
intents.message_content = True
bot = commands.Bot(command_prefix='!', intents=intents)

tenor_link_pattern = re.compile(r'(?:https?://(?:www\.)?)tenor\.(?:com)/view/((?:\w+-)+\d+)', re.IGNORECASE)
message_circle_buffers: Dict[int, Deque[discord.Message | None]] = {}


@bot.event
async def on_message(message: discord.Message):
    if message.author.bot:
        return
    message_circle_buffer = message_circle_buffers.setdefault(message.channel.id, collections.deque(maxlen=int(buffer_length)))

    tenor_match = tenor_link_pattern.search(message.content)

    if tenor_match:
        message_circle_buffer.append(message)
        tenor_count = 0
        for entry in message_circle_buffer:
            if entry is not None:
                tenor_count += 1

        if tenor_count >= int(max_tenor_count):
            await message.channel.send(post_delete_msg)
            
            await message.channel.delete_messages([x for x in message_circle_buffer if x is not None])
    else:
        message_circle_buffer.append(None)


@bot.event
async def on_ready():
    if bot.user is not None:
        print(f'Logged in as {bot.user.name}')

bot.run(bot_token)
