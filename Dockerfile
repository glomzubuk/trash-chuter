# syntax=docker/dockerfile:1
FROM python:3.12.2-alpine

RUN apk add envsubst

ENV BOT_TOKEN 'USE env.list TO SPECIFY TOKENS'
ENV BUFFER_LENGTH '10'
ENV MAX_TENOR_COUNT '3'
ENV POST_DELETE_MSG "Bad users! Bad! Gif spam does not go in this channel! Suffer my wrath!"

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt


COPY ./Launcher.sh ./
COPY ./src/config.py ./config.py.tmpl
COPY ./src/main.py ./

CMD [ "./Launcher.sh" ]
